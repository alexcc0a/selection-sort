package com.nesterov;

public class Main {

    public static void main(String[] args) {
        int[] arr = {64, 34, 25, 12, 22, 11, 90};
        int n = arr.length;

        // Проходим по всем элементам массива.
        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;
            // Ищем минимальный элемент в оставшейся части массива.
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            // Меняем местами текущий элемент и минимальный.
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
        // Выводим отсортированный массив.
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}